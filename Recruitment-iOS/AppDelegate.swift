//
//  AppDelegate.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private var mainCoordinator: MainCoordinator?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {
        let window = UIWindow()
        self.window = window

        let parser = JSONParser()
        let networkingManager = NetworkingManagerImpl(parser: parser)

        let mainCoordinator = MainCoordinator(networkingManager: networkingManager)
        self.mainCoordinator = mainCoordinator
        mainCoordinator.start(inWindow: window)

        return true
    }
}
