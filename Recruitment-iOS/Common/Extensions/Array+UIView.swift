//
//  Array+UIView.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 15/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

extension Array where Element: UIView {
    func addTo(_ view: UIView) {
        for childView in self {
            view.addSubview(childView)
        }
    }
}
