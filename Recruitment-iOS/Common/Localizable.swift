//
//  Localizable.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 20/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

struct Localizable {
    static let start = "Start"
    static let back = "Back"
    static let collection = "Collection"
    static let table = "Table"
}
