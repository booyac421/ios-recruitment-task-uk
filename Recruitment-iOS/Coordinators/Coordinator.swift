//
//  Coordinator.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 15/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit

struct CoordinatorIdentifier: Hashable, Equatable {
    let id = UUID().uuidString
}

protocol Coordinator: class {
    var networkingManager: NetworkingManager { get }
    var id: CoordinatorIdentifier { get }

    init(networkingManager: NetworkingManager, id: CoordinatorIdentifier)
}

protocol CompoundCoordinator: Coordinator {
    associatedtype Controller: UIViewController

    var rootController: Controller? { get }
    var parentCoordinator: Coordinator? { get }
    var children: [CoordinatorIdentifier: Coordinator] { get set }
}

extension CompoundCoordinator {
    func create<Child: Coordinator>() -> Child {
        let coordinator = Child(networkingManager: networkingManager, id: CoordinatorIdentifier())
        children[coordinator.id] = coordinator
        return coordinator
    }

    func destroy<Child: Coordinator>(child: Child) {
        _ = children.removeValue(forKey: child.id)
    }
}
