//
//  JSONParser.swift
//  Route1
//
//  Created by Paweł Sporysz on 11.12.2015.
//  Copyright © 2015 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol Parser: class {
    func decodeArrayFromDict<Object: Decodable>(_ filename: String) -> [Object]
    func decodeObjectFromDict<Object: Decodable>(_ filename: String) -> Object?
}

class JSONParser: Parser {
    func decodeArrayFromDict<Object: Decodable>(_ filename: String) -> [Object] {
        let bundle = Bundle(for: type(of: self))

        guard let path = bundle.path(forResource: filename, ofType: "") else { return [] }

        do {
            let url = URL(fileURLWithPath: path)
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let dict = try decoder.decode([String: [Object]].self, from: data)
            return dict.values.flatMap { $0 }
        } catch {
            print(error)
            return []
        }
    }

    func decodeObjectFromDict<Object: Decodable>(_ filename: String) -> Object? {
        let bundle = Bundle(for: type(of: self))

        guard let path = bundle.path(forResource: filename, ofType: "") else { return nil }

        do {
            let url = URL(fileURLWithPath: path)
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let dict = try decoder.decode([String: Object].self, from: data)
            return dict.values.first
        } catch {
            print(error)
            return nil
        }
    }
}
