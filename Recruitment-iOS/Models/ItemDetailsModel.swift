//
//  ItemDetailsModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import Foundation
import UIKit

struct ItemDetailsModel: Decodable {
    let id: String
    var name: String {
        attributes.name
    }
    var color: UIColor? {
        attributes.color
    }
    var desc: String {
        attributes.desc
    }

    private let attributes: ItemDetailsModelAttributes

    init(id: String, attributes: ItemDetailsModelAttributes) {
        self.id = id
        self.attributes = attributes
    }
}

struct ItemDetailsModelAttributes: Decodable {
    let name: String
    let desc: String
    var color: UIColor? {
        switch colorString {
        case "Red": return UIColor.red
        case "Green": return UIColor.green
        case "Blue": return UIColor.blue
        case "Yellow": return UIColor.yellow
        case "Purple": return UIColor.purple
        default: return nil
        }
    }

    private let colorString: String

    enum CodingKeys: String, CodingKey {
        case name
        case desc
        case colorString = "color"
    }

    init(name: String, desc: String, colorString: String) {
        self.name = name
        self.desc = desc
        self.colorString = colorString
    }
}
