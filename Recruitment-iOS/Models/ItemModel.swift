//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import Foundation
import UIKit

struct ItemModel: Decodable {
    let id: String
    var name: String {
        attributes.name
    }
    var color: UIColor? {
        attributes.color
    }
    var preview: String {
        attributes.preview
    }

    private let attributes: ItemModelAttributes

    init(id: String, attributes: ItemModelAttributes) {
        self.id = id
        self.attributes = attributes
    }
}

struct ItemModelAttributes: Decodable {
    let name: String
    let preview: String
    var color: UIColor? {
        switch colorString {
        case "Red": return UIColor.red
        case "Green": return UIColor.green
        case "Blue": return UIColor.blue
        case "Yellow": return UIColor.yellow
        case "Purple": return UIColor.purple
        default: return nil
        }
    }
    private let colorString: String

    enum CodingKeys: String, CodingKey {
        case name
        case colorString = "color"
        case preview
    }

    init(name: String, preview: String, colorString: String) {
        self.name = name
        self.preview = preview
        self.colorString = colorString
    }
}
