//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol NetworkingManager: class {
    func downloadItems() -> Single<[ItemModel]>
    func downloadItemDetails(withId: String) -> Single<ItemDetailsModel?>
}

class NetworkingManagerImpl: NetworkingManager {
    private let parser: Parser
    private let offset: Double

    init(parser: Parser, offset: Double = 2) {
        self.parser = parser
        self.offset = offset
    }

    func downloadItems() -> Single<[ItemModel]> {
        return requestObjectsArray(filename: "Items.json")
    }

    func downloadItemDetails(withId id: String) -> Single<ItemDetailsModel?> {
        let filename = "Item\(id).json"
        return requestObject(filename: filename)
    }

    private func requestObjectsArray<Object: Decodable>(filename: String) -> Single<[Object]> {
        let dispatchTime = DispatchTime.now() + offset

        return Single<[Object]>.create { result -> Disposable in
            print("Downloading objects array")
            DispatchQueue.main.asyncAfter(deadline: dispatchTime) { [weak self] in
                guard let `self` = self else { return }
                let objects: [Object] = self.parser.decodeArrayFromDict(filename)
                result(.success(objects))
            }

            return Disposables.create()
        }
    }

    private func requestObject<Object: Decodable>(filename: String) -> Single<Object?> {
        let dispatchTime = DispatchTime.now() + offset

        return Single<Object?>.create { result -> Disposable in

            DispatchQueue.main.asyncAfter(deadline: dispatchTime) { [weak self] in
                guard let `self` = self else { return }
                let object: Object? = self.parser.decodeObjectFromDict(filename)
                result(.success(object))
            }

            return Disposables.create()
        }
    }
}
