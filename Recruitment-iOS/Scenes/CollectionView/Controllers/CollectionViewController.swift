//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 17/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import SnapKit
import UIKit
import RxSwift

class CollectionViewController: UIViewController {
    private let collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: UICollectionViewFlowLayout()
    )

    private let viewModel: CollectionViewModel
    private let bag = DisposeBag()

    init(viewModel: CollectionViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    deinit {
        print("Deinit CollectionViewController")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        [collectionView].addTo(view)

        setupLayouts()
        setupStyles()
        setupTableView()
        bindViewModelToView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewWillAppear()
    }
}

// MARK: - Private interface
private extension CollectionViewController {
    func setupLayouts() {
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.right.equalTo(view)
        }
    }

    func setupStyles() {
        view.backgroundColor = .white
        collectionView.backgroundColor = .white
    }

    func setupTableView() {
        viewModel.setupCollection(collectionView: self.collectionView)
    }

    func bindViewModelToView() {}
}
