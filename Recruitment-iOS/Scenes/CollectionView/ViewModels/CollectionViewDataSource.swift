//
//  CollectionViewDataSource.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 17/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa

protocol CollectionViewDataSource: class {
    var selectedItem: Driver<Int> { get }

    func setupCollection(_ collectionView: UICollectionView)
    func loadData(_ itemModels: [ItemModel])
}

private struct Layouts {
    static let edgeInset: CGFloat = 50
    static let offsetBetweenCells: CGFloat = 25
}

class CollectionViewDataSourceImpl: NSObject, CollectionViewDataSource {
    var selectedItem: Driver<Int> {
        return selectedItemRelay
            .asDriver(onErrorRecover: { _ in Driver.never() })
            .throttle(.seconds(1), latest: false)
    }

    private let selectedItemRelay = PublishRelay<Int>()
    private var itemModels = [ItemModel]()
    private var collectionView: UICollectionView?

    func setupCollection(_ collectionView: UICollectionView) {
        self.collectionView = collectionView
        collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.allowsMultipleSelection = false
    }

    func loadData(_ itemModels: [ItemModel]) {
        self.itemModels = itemModels
        DispatchQueue.main.async { [weak self] in
            self?.collectionView?.reloadData()
        }
    }

    deinit {
        print("Deinit CollectionViewDataSourceImpl")
    }
}

extension CollectionViewDataSourceImpl: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemModels.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let optionalCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CollectionViewCell.identifier,
            for: indexPath
        ) as? CollectionViewCell

        guard let cell = optionalCell else { return UICollectionViewCell() }

        let itemModel = itemModels[indexPath.row]
        cell.setup(withItemModel: itemModel)

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedItemRelay.accept(indexPath.item)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        let offsetSum = 2 * Layouts.edgeInset + Layouts.offsetBetweenCells
        let cellSize = (screenWidth - offsetSum) / 2

        return CGSize(width: cellSize, height: cellSize)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: Layouts.edgeInset,
                            left: Layouts.edgeInset,
                            bottom: Layouts.edgeInset,
                            right: Layouts.edgeInset)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Layouts.offsetBetweenCells
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Layouts.offsetBetweenCells
    }
}
