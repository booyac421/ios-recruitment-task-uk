//
//  CollectionViewModel.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 17/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol CollectionViewModelDelegate: class {
    func didSelectItem(_ itemModel: ItemModel)
}

protocol CollectionViewModel: class {
    func viewWillAppear()

    func setupCollection(collectionView: UICollectionView)
}

class CollectionViewModelImpl: CollectionViewModel {

    private let networkingManager: NetworkingManager
    private let dataSource: CollectionViewDataSource
    private let bag = DisposeBag()
    private var itemModels = [ItemModel]()

    private weak var delegate: CollectionViewModelDelegate?

    init(
        networkingManager: NetworkingManager,
        dataSource: CollectionViewDataSource = CollectionViewDataSourceImpl(),
        delegate: CollectionViewModelDelegate? = nil
    ) {
        self.networkingManager = networkingManager
        self.dataSource = dataSource
        self.delegate = delegate
        self.setup()
    }

    deinit {
        print("Deinit CollectionViewModelImpl")
    }
}

// MARK: Public interface
extension CollectionViewModelImpl {
    func viewWillAppear() {
        fetchItemModels()
    }

    func setupCollection(collectionView: UICollectionView) {
        dataSource.setupCollection(collectionView)
    }
}

// MARK: - Private interface
private extension CollectionViewModelImpl {
    func setup() {
        dataSource.selectedItem.drive(
            onNext: { [weak self] index in
                guard let itemModel = self?.itemModels[index] else { return }
                self?.delegate?.didSelectItem(itemModel)
            }
        )
        .disposed(by: bag)
    }

    func fetchItemModels() {
        networkingManager
            .downloadItems()
            .subscribe(
                onSuccess: { [weak self] itemModels in
                    self?.itemModels = itemModels
                    self?.dataSource.loadData(itemModels)
                }
            )
            .disposed(by: bag)
    }
}
