//
//  CollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 17/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import SnapKit
import UIKit

class CollectionViewCell: UICollectionViewCell {
    static let identifier = "CollectionViewCell"

    private let label = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        [label].addTo(self)

        setupLayouts()
        setupStyles()
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.label.text = ""
    }
}

// MARK: - Public interface
extension CollectionViewCell {
    func setup(withItemModel itemModel: ItemModel) {
        backgroundColor = itemModel.color
        label.text = itemModel.name
    }
}

// MARK: - Private interface
private extension CollectionViewCell {
    func setupLayouts() {
        label.snp.makeConstraints { make in
            make.centerY.equalTo(self)
            make.centerX.equalTo(self)
        }
    }

    func setupStyles() {
        label.textAlignment = .center
        label.textColor = .black
    }
}
