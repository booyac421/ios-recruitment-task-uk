//
//  CustomTabBarController.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 17/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {

    private let viewModel: CustomTabBarViewModel

    init(viewModel: CustomTabBarViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem(title: Localizable.back,
                                         style: .plain,
                                         target: self,
                                         action: #selector(didTapBack))
        navigationItem.leftBarButtonItem = backButton
    }
}

@objc private extension CustomTabBarController {
    func didTapBack() {
        viewModel.requestDismiss()
    }
}
