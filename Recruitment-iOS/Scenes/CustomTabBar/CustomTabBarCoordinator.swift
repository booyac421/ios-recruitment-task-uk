//
//  CustomTabBarCoordinator.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 15/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol CustomTabBarCoordinatorDelegate: class {
    func didClose(coordinator: CustomTabBarCoordinator)
}

class CustomTabBarCoordinator: CompoundCoordinator {
    let id: CoordinatorIdentifier
    let networkingManager: NetworkingManager
    var children = [CoordinatorIdentifier: Coordinator]()
    var rootController: UINavigationController?
    var parentCoordinator: Coordinator?

    weak var delegate: CustomTabBarCoordinatorDelegate?

    required init(networkingManager: NetworkingManager, id: CoordinatorIdentifier = CoordinatorIdentifier()) {
        self.networkingManager = networkingManager
        self.id = id
    }

    deinit {
        print("Deinit TabBarCoordinator")
    }

    func start(inController controller: UINavigationController) {
        let VM = CustomTabBarViewModelImpl(delegate: self)
        let VC = CustomTabBarController(viewModel: VM)
        startTableView(inController: VC)
        startCollectionView(inController: VC)

        self.rootController = controller
        controller.pushViewController(VC, animated: true)
    }

    private func startTableView(inController controller: UITabBarController) {
        let VM = TableViewModelImpl(networkingManager: networkingManager, delegate: self)
        let VC = TableViewController(viewModel: VM)
        VC.tabBarItem.title = Localizable.table
        controller.addChild(VC)
    }

    private func startCollectionView(inController controller: UITabBarController) {
        let VM = CollectionViewModelImpl(networkingManager: networkingManager, delegate: self)
        let VC = CollectionViewController(viewModel: VM)
        VC.tabBarItem.title = Localizable.collection
        controller.addChild(VC)
    }

    private func startDetailsView(forItemModel itemModel: ItemModel) {
        guard let NVC = rootController else {
            fatalError("Root controller not initialised")
        }

        let VM = DetailsViewModelImpl(itemModel: itemModel, networkingManager: networkingManager, delegate: self)
        let VC = DetailsViewController(viewModel: VM)
        NVC.pushViewController(VC, animated: false)
    }
}

extension CustomTabBarCoordinator: CustomTabBarViewModelDelegate {
    func requestDismissToMain() {
        delegate?.didClose(coordinator: self)
    }
}

extension CustomTabBarCoordinator: TableViewModelDelegate, CollectionViewModelDelegate {
    func didSelectItem(_ itemModel: ItemModel) {
        startDetailsView(forItemModel: itemModel)
    }
}

extension CustomTabBarCoordinator: DetailsViewModelDelegate {
    func requestDismissToTabbar() {
        rootController?.popViewController(animated: true)
    }
}
