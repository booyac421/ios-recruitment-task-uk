//
//  CustomTabBarViewModel.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 17/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol CustomTabBarViewModelDelegate: class {
    func requestDismissToMain()
}

protocol CustomTabBarViewModel: class {
    func requestDismiss()
}

class CustomTabBarViewModelImpl: CustomTabBarViewModel {
    private weak var delegate: CustomTabBarViewModelDelegate?

    init(delegate: CustomTabBarViewModelDelegate? = nil) {
        self.delegate = delegate
    }

    func requestDismiss() {
        delegate?.requestDismissToMain()
    }
}
