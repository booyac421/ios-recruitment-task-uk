//
//  DetailsViewController.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 16/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class DetailsViewController: UIViewController {
    private let textView = UITextView()

    private let viewModel: DetailsViewModel
    private let bag = DisposeBag()

    init(viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    deinit {
        print("Deinit DetailsViewController")
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func viewDidLoad() {
        super.viewDidLoad()
        [textView].addTo(view)

        setupLayouts()
        setupStyles()
        bindViewModelToView()
    }
}

// MARK: - Private interface
private extension DetailsViewController {
    func setupLayouts() {
        textView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.right.equalTo(view)
        }
    }

    func setupStyles() {
        textView.backgroundColor = .clear
        textView.isUserInteractionEnabled = false

        let backButton = UIBarButtonItem(title: Localizable.back,
                                         style: .plain,
                                         target: self,
                                         action: #selector(didTapBack))
        navigationItem.leftBarButtonItem = backButton
    }

    func bindViewModelToView() {
        viewModel.title
            .drive(onNext: { [weak self] title in self?.title = title })
            .disposed(by: bag)

        viewModel.color.drive(view.rx.backgroundColor).disposed(by: bag)

        viewModel.text.drive(textView.rx.text).disposed(by: bag)
    }
}

// MARK: - Selectors
@objc private extension DetailsViewController {
    func didTapBack() {
        viewModel.requestDismiss()
    }
}
