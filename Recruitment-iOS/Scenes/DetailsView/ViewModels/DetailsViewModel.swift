//
//  DetailsViewModel.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 16/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol DetailsViewModelDelegate: class {
    func requestDismissToTabbar()
}

protocol DetailsViewModel: class {
    var title: Driver<String> { get }
    var color: Driver<UIColor?> { get }
    var text: Driver<String> { get }

    func requestDismiss()
}

class DetailsViewModelImpl: DetailsViewModel {
    var title: Driver<String> {
        return titleRelay.asDriver()
    }
    var color: Driver<UIColor?> {
        return colorRelay.asDriver()
    }
    var text: Driver<String> {
        return textRelay.asDriver()
    }

    private let titleRelay = BehaviorRelay<String>(value: "")
    private let colorRelay = BehaviorRelay<UIColor?>(value: nil)
    private let textRelay = BehaviorRelay<String>(value: "")
    private let bag = DisposeBag()
    private let networkingManager: NetworkingManager
    private var itemModel: ItemModel

    private weak var delegate: DetailsViewModelDelegate?

    init(itemModel: ItemModel,
         networkingManager: NetworkingManager,
         delegate: DetailsViewModelDelegate? = nil) {
        self.itemModel = itemModel
        self.networkingManager = networkingManager
        self.delegate = delegate
        self.setup()
    }

    deinit {
        print("Deinit DetailsViewModelImpl")
    }
}

// MARK: Public interface
extension DetailsViewModelImpl {
    func requestDismiss() {
        delegate?.requestDismissToTabbar()
    }
}

// MARK: - Private interface
private extension DetailsViewModelImpl {
    func setup() {
        self.colorRelay.accept(itemModel.color)

        let title = itemModel.name
        let newTitle = title.enumerated().map { index, letter -> String in
            print(index)
            return (index + 1) % 2 == 0 ? String(letter).lowercased() : String(letter).uppercased()
        }
        .joined()
        self.titleRelay.accept(newTitle)

        self.fetchItemDetailsModel(withId: itemModel.id)
    }

    func fetchItemDetailsModel(withId id: String) {
        networkingManager
            .downloadItemDetails(withId: id)
            .subscribe(
                onSuccess: { [weak self] itemDetails in
                    self?.textRelay.accept(itemDetails?.desc ?? "")
                }
            )
            .disposed(by: bag)
    }
}
