//
//  MainViewController.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 15/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import SnapKit
import UIKit
import RxSwift
import RxCocoa

class MainViewController: UIViewController {
    private let startButton = UIButton()

    private let viewModel: MainViewModel
    private let bag = DisposeBag()

    init(viewModel: MainViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func viewDidLoad() {
        super.viewDidLoad()
        [startButton].addTo(view)

        setupLayouts()
        setupStyles()
        bindViewModelToView()
    }
}

// MARK: - Private interface
private extension MainViewController {
    func setupLayouts() {
        startButton.snp.makeConstraints { [weak self] make in
            guard let `self` = self else { return }
            make.center.equalTo(self.view)
        }
    }

    func setupStyles() {
        view.backgroundColor = .white
        startButton.setTitle(Localizable.start.uppercased(), for: .normal)
        startButton.setTitleColor(.systemBlue, for: .normal)
    }

    func bindViewModelToView() {
        startButton.rx.tap
            .asDriver()
            .throttle(.seconds(1), latest: false)
            .drive(onNext: { [weak self] in self?.viewModel.tapStartButton() })
            .disposed(by: bag)
    }
}
