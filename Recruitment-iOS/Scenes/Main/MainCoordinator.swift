//
//  MainCoordinator.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 15/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class MainCoordinator: CompoundCoordinator {
    let id: CoordinatorIdentifier
    let networkingManager: NetworkingManager
    var children = [CoordinatorIdentifier: Coordinator]()
    var rootController: UINavigationController?
    var parentCoordinator: Coordinator?

    required init(networkingManager: NetworkingManager, id: CoordinatorIdentifier = CoordinatorIdentifier()) {
        self.networkingManager = networkingManager
        self.id = id
    }

    func start(inWindow window: UIWindow) {
        let VM = MainViewModelImpl(delegate: self)
        let VC = MainViewController(viewModel: VM)
        let NVC = UINavigationController(rootViewController: VC)
        self.rootController = NVC

        window.rootViewController = NVC
        window.makeKeyAndVisible()
    }

    func startTabBarCoordinator() {
        guard let rootController = self.rootController else {
            fatalError("Main controller not initialised")
        }
        let coordinator: CustomTabBarCoordinator = create()
        coordinator.delegate = self
        coordinator.start(inController: rootController)
    }
}

extension MainCoordinator: MainViewModelDelegate {
    func didTapStartButton() {
        startTabBarCoordinator()
    }
}

extension MainCoordinator: CustomTabBarCoordinatorDelegate {
    func didClose(coordinator: CustomTabBarCoordinator) {
        rootController?.popViewController(animated: true)
        destroy(child: coordinator)
        children.removeValue(forKey: coordinator.id)
    }
}
