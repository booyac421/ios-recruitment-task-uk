//
//  MainViewModel.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 15/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol MainViewModelDelegate: class {
    func didTapStartButton()
}

protocol MainViewModel: class {
    func tapStartButton()
}

class MainViewModelImpl: MainViewModel {
    private weak var delegate: MainViewModelDelegate?

    init(delegate: MainViewModelDelegate? = nil) {
        self.delegate = delegate
    }
}

// MARK: Public interface
extension MainViewModelImpl {
    func tapStartButton() {
        delegate?.didTapStartButton()
    }
}
