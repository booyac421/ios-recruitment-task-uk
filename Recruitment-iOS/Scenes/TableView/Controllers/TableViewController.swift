//
//  TableViewController.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 15/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import SnapKit
import UIKit
import RxSwift

class TableViewController: UIViewController {
    private let tableView = UITableView()

    private let viewModel: TableViewModel
    private let bag = DisposeBag()

    init(viewModel: TableViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    deinit {
        print("Deinit TableViewController")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        [tableView].addTo(view)

        setupLayouts()
        setupStyles()
        setupTableView()
        bindViewModelToView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewWillAppear()
    }
}

// MARK: - Private interface
private extension TableViewController {
    func setupLayouts() {
        tableView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.right.equalTo(view)
        }
    }

    func setupStyles() {
        view.backgroundColor = .white
        tableView.backgroundColor = .white
    }

    func setupTableView() {
        viewModel.setupTable(tableView: self.tableView)
    }

    func bindViewModelToView() {}
}
