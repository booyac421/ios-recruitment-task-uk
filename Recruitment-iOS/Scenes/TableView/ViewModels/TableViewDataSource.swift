//
//  TableViewDataSource.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 15/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa

protocol TableViewDataSource: class {
    var selectedItem: Driver<Int> { get }

    func setupTable(_ tableView: UITableView)
    func loadData(_ itemModels: [ItemModel])
}

class TableViewDataSourceImpl: NSObject, TableViewDataSource {
    var selectedItem: Driver<Int> {
        return selectedItemRelay
            .asDriver(onErrorRecover: { _ in Driver.never() })
            .throttle(.seconds(1), latest: false)
    }

    private let selectedItemRelay = PublishRelay<Int>()
    private var itemModels = [ItemModel]()
    private var tableView: UITableView?

    func setupTable(_ tableView: UITableView) {
        self.tableView = tableView
        tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsMultipleSelection = false
        tableView.rowHeight = UITableView.automaticDimension
    }

    func loadData(_ itemModels: [ItemModel]) {
        self.itemModels = itemModels
        DispatchQueue.main.async { [weak self] in
            self?.tableView?.reloadData()
        }
    }

    deinit {
        print("Deinit TableViewDataSourceImpl")
    }
}

extension TableViewDataSourceImpl: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let dequedCell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier,
                                                       for: indexPath) as? TableViewCell
        guard let cell = dequedCell else { return UITableViewCell() }

        let itemModel = itemModels[indexPath.row]
        cell.setup(withItemModel: itemModel)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItemRelay.accept(indexPath.item)
    }
}
