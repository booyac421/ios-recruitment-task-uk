//
//  TableViewModel.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 15/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol TableViewModelDelegate: class {
    func didSelectItem(_ itemModel: ItemModel)
}

protocol TableViewModel: class {
    func viewWillAppear()

    func setupTable(tableView: UITableView)
}

class TableViewModelImpl: TableViewModel {

    private let networkingManager: NetworkingManager
    private let dataSource: TableViewDataSource
    private let bag = DisposeBag()
    private var itemModels = [ItemModel]()

    private weak var delegate: TableViewModelDelegate?

    init(
        networkingManager: NetworkingManager,
        dataSource: TableViewDataSource = TableViewDataSourceImpl(),
        delegate: TableViewModelDelegate? = nil
    ) {
        self.networkingManager = networkingManager
        self.dataSource = dataSource
        self.delegate = delegate
        self.setup()
    }

    deinit {
        print("Deinit TableViewModelImpl")
    }
}

// MARK: Public interface
extension TableViewModelImpl {
    func viewWillAppear() {
        fetchItemModels()
    }

    func setupTable(tableView: UITableView) {
        dataSource.setupTable(tableView)
    }
}

// MARK: - Private interface
private extension TableViewModelImpl {
    func setup() {
        dataSource.selectedItem.drive(
            onNext: { [weak self] index in
                guard let itemModel = self?.itemModels[index] else { return }
                self?.delegate?.didSelectItem(itemModel)
            }
        )
        .disposed(by: bag)
    }

    func fetchItemModels() {
        networkingManager
            .downloadItems()
            .subscribe(
                onSuccess: { [weak self] itemModels in
                    self?.itemModels = itemModels
                    self?.dataSource.loadData(itemModels)
                }
            )
            .disposed(by: bag)
    }
}
