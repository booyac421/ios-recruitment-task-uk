//
//  TableViewCell.swift
//  Recruitment-iOS
//
//  Created by Krystian Bujak on 18/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation
import SnapKit
import UIKit

class TableViewCell: UITableViewCell {
    static let identifier = "TableViewCell"

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)

        setupStyles()
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func prepareForReuse() {
        super.prepareForReuse()
        textLabel?.text = nil
        detailTextLabel?.text = nil
    }
}

// MARK: - Public interface
extension TableViewCell {
    func setup(withItemModel itemModel: ItemModel) {
        backgroundColor = itemModel.color
        textLabel?.text = itemModel.name
        detailTextLabel?.text = itemModel.preview
    }
}

// MARK: - Private interface
private extension TableViewCell {
    func setupStyles() {
        detailTextLabel?.numberOfLines = 0
        selectionStyle = .none
    }
}
