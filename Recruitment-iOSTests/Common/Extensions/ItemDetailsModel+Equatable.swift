//
//  ItemDetailsModel+Equatable.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 18/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

extension ItemDetailsModel: Equatable {
    static func == (lhs: ItemDetailsModel, rhs: ItemDetailsModel) -> Bool {
        return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.desc == rhs.desc
            && lhs.color == rhs.color
    }
}
