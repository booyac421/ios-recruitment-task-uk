//
//  ItemModel+Equatable.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 18/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

extension ItemModel: Equatable {
    static func == (lhs: ItemModel, rhs: ItemModel) -> Bool {
        return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.preview == rhs.preview
            && lhs.color == rhs.color
    }
}
