//
//  NetworkingManagerMock.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 19/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import RxSwift
@testable import Recruitment_iOS

class NetworkingManagerMock: NetworkingManager {

    var downloadItemsCallsCount: Int = 0
    var downloadItemsClosure: (() -> Single<[ItemModel]>)?
    func downloadItems() -> Single<[ItemModel]> {
        downloadItemsCallsCount += 1
        return downloadItemsClosure?() ?? Single.just([itemModelStub1, itemModelStub2])
    }

    var downloadItemDetailsCallsCount: Int = 0
    var downloadItemDetailsParameter: String?
    var downloadItemDetailsClosure: ((String) -> Single<ItemDetailsModel?>)?
    func downloadItemDetails(withId id: String) -> Single<ItemDetailsModel?> {
        downloadItemDetailsCallsCount += 1
        downloadItemDetailsParameter = id
        return downloadItemDetailsClosure?(id) ?? Single.just(itemDetailsModelStub1)
    }
}
