//
//  Mocks.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 19/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS

class ParserMock: Parser {
    private let itemModelStubs: [ItemModel]
    private let itemDetailsModelStubs: [ItemDetailsModel]

    init(itemModelStubs: [ItemModel], itemDetailsModelStubs: [ItemDetailsModel]) {
        self.itemModelStubs = itemModelStubs
        self.itemDetailsModelStubs = itemDetailsModelStubs
    }

    var decodeArrayFromDictCallsCount: Int = 0
    var decodeArrayFromDictParameter: String?
    func decodeArrayFromDict<Object>(_ filename: String) -> [Object] where Object: Decodable {
        decodeArrayFromDictParameter = filename
        decodeArrayFromDictCallsCount += 1
        return (itemModelStubs as? [Object]) ?? []
    }

    var decodeObjectFromDictCallsCount: Int = 0
    var decodeObjectFromDictParameter: String?
    func decodeObjectFromDict<Object>(_ filename: String) -> Object? where Object: Decodable {
        decodeObjectFromDictParameter = filename
        decodeObjectFromDictCallsCount += 1
        return itemDetailsModelStubs.first(where: { "Item\($0.id).json" == filename }) as? Object
    }
}
