//
//  ItemDetailsModelStubs.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 18/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

let itemDetailsModelStub1 = ItemDetailsModel(
    id: "1",
    attributes: ItemDetailsModelAttributes(name: "ItemDetailsStub1",
                                           desc: "Test description 1",
                                           colorString: "Red")
)

let itemDetailsModelStub2 = ItemDetailsModel(
    id: "2",
    attributes: ItemDetailsModelAttributes(name: "ItemDetailsStub2",
                                           desc: "Test description 2",
                                           colorString: "Green")
)
