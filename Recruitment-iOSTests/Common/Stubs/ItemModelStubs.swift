//
//  ItemModelStubs.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 18/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

let itemModelStub1 = ItemModel(
    id: "1",
    attributes: ItemModelAttributes(name: "ItemStub1",
                                    preview: "Test preview 1",
                                    colorString: "Red")
)

let itemModelStub2 = ItemModel(
    id: "2",
    attributes: ItemModelAttributes(name: "ItemStub2",
                                    preview: "Test preview 2",
                                    colorString: "Green")
)
