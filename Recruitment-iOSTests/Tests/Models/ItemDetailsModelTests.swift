//
//  ItemDetailsModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 19/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
import Nimble
@testable import Recruitment_iOS

class ItemDetailsModelTests: XCTestCase {

    func testInit_whenCorrectColorString_thenColorIsCorrect() {
        let expectedColor = UIColor.red
        let itemDetailsModelAttributes = createAttributes(withColorString: "Red")
        let itemDetailsModel = ItemDetailsModel(id: "1", attributes: itemDetailsModelAttributes)

        expect(expectedColor == itemDetailsModel.color).to(beTrue())
    }

    func testInit_whenInorrectColorString_thenColorIsNil() {
        let itemDetailsModelAttributes = createAttributes(withColorString: "IncorrectColor")
        let itemDetailsModel = ItemDetailsModel(id: "1", attributes: itemDetailsModelAttributes)

        expect(itemDetailsModel.color).to(beNil())
    }

    private func createAttributes(withColorString colorString: String) -> ItemDetailsModelAttributes {
        return ItemDetailsModelAttributes(name: "Test item model",
                                          desc: "Test description",
                                          colorString: colorString)
    }
}
