//
//  ItemModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 19/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
import Nimble
@testable import Recruitment_iOS

class ItemModelTests: XCTestCase {

    func testInit_whenCorrectColorString_thenColorIsCorrect() {
        let expectedColor = UIColor.red
        let itemModelAttributes = createAttributes(withColorString: "Red")
        let itemModel = ItemModel(id: "1", attributes: itemModelAttributes)

        expect(expectedColor == itemModel.color).to(beTrue())
    }

    func testInit_whenInorrectColorString_thenColorIsNil() {
        let itemModelAttributes = createAttributes(withColorString: "IncorrectColor")
        let itemModel = ItemModel(id: "1", attributes: itemModelAttributes)

        expect(itemModel.color).to(beNil())
    }

    private func createAttributes(withColorString colorString: String) -> ItemModelAttributes {
        return ItemModelAttributes(name: "Test item model",
                                   preview: "Test preview",
                                   colorString: colorString)
    }
}
