//
//  JSONParserTests.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 18/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
import Nimble
@testable import Recruitment_iOS

class JSONParserTests: XCTestCase {
    private var jsonParser: JSONParser!

    override func setUp() {
        jsonParser = JSONParser()
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
        jsonParser = nil
    }

    func testDecodeArrayFromDict_whenFileDoesNotExist_thenResultsBeEmpty() {
        let objects: [ItemModel] = jsonParser.decodeArrayFromDict("NotExistingFile.json")
        expect(objects).to(beEmpty())
    }

    func testDecodeArrayFromDict_whenFileHasWrongFormat_thenResultsBeEmpty() {
        let objects: [ItemModel] = jsonParser.decodeArrayFromDict("Item1.json")
        expect(objects).to(beEmpty())
    }

    func testDecodeObjectFromDict_whenFileDoesNotExist_thenObjectNil() {
        let object: ItemDetailsModel? = jsonParser.decodeObjectFromDict("NotExistingFile.json")
        expect(object).to(beNil())
    }

    func testDecodeObjectFromDict_whenFileHasWrongFormat_thenObjectNil() {
        let object: ItemDetailsModel? = jsonParser.decodeObjectFromDict("Items.json")
        expect(object).to(beNil())
    }

    func testParsingItemModelArray() {
        let expectedObjects = [itemModelStub1, itemModelStub2]
        let actualObjects: [ItemModel] = jsonParser.decodeArrayFromDict("ItemModelStubs.json")
        expect(expectedObjects == actualObjects).to(beTrue())
    }

    func testParsingItemModel() {
        let expectedObject: ItemModel? = itemModelStub1
        let actualObject: ItemModel? = jsonParser.decodeObjectFromDict("ItemModelStub1.json")
        expect(expectedObject == actualObject).to(beTrue())
    }

    func testParsingItemDetailsModelArray() {
        let expectedObjects = [itemDetailsModelStub1, itemDetailsModelStub2]
        let actualObjects: [ItemDetailsModel] = jsonParser.decodeArrayFromDict("ItemDetailsStubs.json")
        expect(expectedObjects == actualObjects).to(beTrue())
    }

    func testParsingItemDetailsModel() {
        let expectedObject: ItemDetailsModel? = itemDetailsModelStub1
        let actualObject: ItemDetailsModel? = jsonParser.decodeObjectFromDict("ItemDetailsStub1.json")
        expect(expectedObject == actualObject).to(beTrue())
    }
}
