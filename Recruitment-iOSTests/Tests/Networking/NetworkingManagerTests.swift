//
//  NetworkingManagerTests.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 19/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
import Nimble
import RxBlocking
@testable import Recruitment_iOS

class NetworkingManagerTests: XCTestCase {

    var parser: ParserMock!
    var networkingManager: NetworkingManager!

    override func setUp() {
        super.setUp()
        self.parser = ParserMock(itemModelStubs: [itemModelStub1, itemModelStub2],
                                 itemDetailsModelStubs: [itemDetailsModelStub1, itemDetailsModelStub2])
        self.networkingManager = NetworkingManagerImpl(parser: parser, offset: 0)
    }

    override func tearDown() {
        self.parser = nil
        self.networkingManager = nil
        super.tearDown()
    }

    func testDownloadItemsMethod() {
        let expectedItems = [itemModelStub1, itemModelStub2]

        let actualItems = try? networkingManager.downloadItems().toBlocking().single()

        expect(expectedItems == actualItems).to(beTrue())
        expect(self.parser.decodeArrayFromDictCallsCount).to(be(1))
        expect(self.parser.decodeArrayFromDictParameter).to(be("Items.json"))
    }

    func testDownloadItemDetailsMethod_whenCorrectId_thenTrue() {
        let expectedItem = itemDetailsModelStub1

        let actualItem = try? networkingManager.downloadItemDetails(withId: "1").toBlocking().single()

        expect(expectedItem == actualItem).to(beTrue())
        expect(self.parser.decodeObjectFromDictCallsCount).to(be(1))
        expect(self.parser.decodeObjectFromDictParameter).to(be("Item1.json"))
    }

    func testDownloadItemDetailsMethod_whenIncorrectId_thenResultIsNil() {
        let actualItem = try? networkingManager.downloadItemDetails(withId: "123").toBlocking().single()

        expect(actualItem).to(beNil())
    }
}
