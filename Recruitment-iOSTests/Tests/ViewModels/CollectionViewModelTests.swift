//
//  CollectionViewModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 19/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
import Nimble
import RxSwift
import RxBlocking
import RxTest
@testable import Recruitment_iOS

class CollectionViewModelTests: XCTestCase {

    var networkingManager: NetworkingManagerMock!
    var viewModel: CollectionViewModel!
    var bag: DisposeBag!
    var scheduler: TestScheduler!

    override func setUp() {
        super.setUp()
        self.networkingManager = NetworkingManagerMock()
        self.viewModel = CollectionViewModelImpl(networkingManager: networkingManager)
        self.bag = DisposeBag()
        self.scheduler = TestScheduler(initialClock: 0)
    }

    override func tearDown() {
        self.viewModel = nil
        self.networkingManager = nil
        self.bag = nil
        self.scheduler = nil
        super.tearDown()
    }

    func testNetworkManagerDownloadItems_whenViewWillAppearNotCalled_thenCallsCountIs0() {
        expect(self.networkingManager.downloadItemsCallsCount).to(be(0))
    }

    func testNetworkManagerDownloadItems_whenViewWillAppearCalled_thenCallsCountIs1() {
        viewModel.viewWillAppear()
        expect(self.networkingManager.downloadItemsCallsCount).to(be(1))
    }
}
