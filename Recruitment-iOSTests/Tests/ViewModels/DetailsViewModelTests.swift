//
//  DetailsViewModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Krystian Bujak on 19/05/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
import Nimble
import RxSwift
import RxBlocking
import RxTest
@testable import Recruitment_iOS

class DetailsViewModelTests: XCTestCase {

    var networkingManager: NetworkingManagerMock!
    var viewModel: DetailsViewModel!
    var bag: DisposeBag!
    var scheduler: TestScheduler!

    override func setUp() {
        super.setUp()
        self.networkingManager = NetworkingManagerMock()
        self.viewModel = DetailsViewModelImpl(itemModel: itemModelStub1, networkingManager: networkingManager)
        self.bag = DisposeBag()
        self.scheduler = TestScheduler(initialClock: 0)
    }

    override func tearDown() {
        self.viewModel = nil
        self.networkingManager = nil
        self.bag = nil
        self.scheduler = nil
        super.tearDown()
    }

    func testColorDriver() {
        let expectedColor = UIColor.red
        let color = scheduler.createObserver(UIColor?.self)

        viewModel.color.drive(color).disposed(by: bag)
        let actualColor = color.events.first?.value.element

        expect(expectedColor == actualColor).to(beTrue())
    }

    func testTitleDriver() {
        let expectedTitle = "ItEmStUb1"
        let title = scheduler.createObserver(String.self)

        viewModel.title.drive(title).disposed(by: bag)
        let actualTitle = title.events.first?.value.element

        expect(expectedTitle == actualTitle).to(beTrue())
    }

    func testTextDriver() {
        let expectedText = "Test description 1"
        let text = scheduler.createObserver(String.self)

        viewModel.text.drive(text).disposed(by: bag)
        let actualText = text.events.first?.value.element

        expect(expectedText == actualText).to(beTrue())
    }

    func testNetworkManagerCallsDownloadItemDetails() {
        let text = scheduler.createObserver(String.self)

        viewModel.text.drive(text).disposed(by: bag)

        expect(self.networkingManager.downloadItemDetailsCallsCount).to(be(1))
        expect(self.networkingManager.downloadItemDetailsParameter).to(be("1"))
    }
}
